package dk.kea.swd.passapp;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

/**
 * Created by Cris on 21-Sep-15.
 */
public class DBQueries {
    private static Connection connection;
    public static ObservableList<Account> getAccounts(int userID) {
        ObservableList<Account> list = FXCollections.observableArrayList();
        try {
            connection = DBConnector.getConnection();
            String sql = "SELECT * FROM account WHERE user_id_fk = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1,userID);
            stmt.executeQuery();

            ResultSet rs = stmt.getResultSet();
            while(rs.next()) {
                String url = rs.getString("url");
                String website = rs.getString("website");
                String decodedUsername = Encoding.decode(rs.getString("username"));
                String decodedPass = Encoding.decode(rs.getString("pass"));
                int accountID = rs.getInt("account_id");


                Account a = new Account(accountID,userID,url,website,decodedUsername,decodedPass);
                list.add(a);
                System.out.println(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void createAccount(Account newAccount) {
        try {
            connection= DBConnector.getConnection();

            String encodedUsername = Encoding.encode(newAccount.getUsername());
            String encodedPass = Encoding.encode(newAccount.getPass());

            String sql = "INSERT INTO account (user_id_fk, url, website, username, pass) VALUES (?,?,?,?,?);";

            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, newAccount.getUserID());
            stmt.setString(2, newAccount.getUrl());
            stmt.setString(3, newAccount.getWebsite());
            stmt.setString(4, encodedUsername);
            stmt.setString(5, encodedPass);

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            newAccount.setAccountID(rs.getInt(1));

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void editAccount(Account a) {
        try {
            connection = DBConnector.getConnection();

            String encodedUsername = Encoding.encode(a.getUsername());
            String encodedPass = Encoding.encode(a.getPass());
            String sql = "UPDATE account SET url = ?, website = ?, username = ?, pass = ? WHERE account_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,a.getUrl());
            preparedStatement.setString(2,a.getWebsite());
            preparedStatement.setString(3,encodedUsername);
            preparedStatement.setString(4, encodedPass);
            preparedStatement.setInt(5, a.getUserID());
            preparedStatement.execute();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void deleteAccount(Account a) {
        try {
            connection = DBConnector.getConnection();

            String sql = "DELETE FROM account WHERE account_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,a.getAccountID());
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static int checkUserExistance(String user) {
        try {
            connection = DBConnector.getConnection();
            String sql = "SELECT user_id FROM user WHERE username = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1,user);
            stmt.executeQuery();
            int userId = 0;
            ResultSet rs = stmt.getResultSet();
            if (rs.next()) {
                userId = rs.getInt("user_id");
            }
            System.out.println("We have user?: " + ((userId > 0)? "true" : "false"));
            return userId;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static boolean checkPassWord(String user, String pw) {
        boolean result = false;
        int id         = 0;
        try {
            connection = DBConnector.getConnection();
            String sql = "SELECT * FROM user WHERE username = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, user);
            stmt.executeQuery();
            String pass = "";
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                pass = rs.getString("pass");
                id   = rs.getInt("user_id");
            }
            if (pass.equals(pw)){
                result        = true;
                Login.user_id = id;
            }
            System.out.println("We have right password?: " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void createNewUser(String user, String pass, String email) {
        try {
            connection= DBConnector.getConnection();
            String sql 	= "INSERT INTO user (username, pass, email) " +
                    "VALUES (?,?,?);";

            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, user);
            stmt.setString(2, pass);
            stmt.setString(3, email);

            stmt.execute();

            System.out.println("user created");
        } catch (SQLException e) {
            System.err.println("User NOT created");
            e.printStackTrace();
        }
    }
}
