package dk.kea.swd.passapp;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.*;
import javafx.scene.paint.Color;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.util.Callback;
import javafx.scene.control.Button;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

public class AccountsPageController implements Initializable {

    @FXML private TableView<Account> accountTable;
    @FXML private TableColumn<Account, String> websiteColumn;
    @FXML private TableColumn<Account, String> urlColumn;
    @FXML private TableColumn<Account, String> usernameColumn;
    @FXML private TableColumn<Account, String> passColumn;
    @FXML private TableColumn<Account, String> deleteColumn;

    @FXML private TextField websiteTF;
    @FXML private TextField usernameTF;
    @FXML private TextField passTF;
    @FXML private TextField urlTF;
    @FXML private Button btnSave;

    private ObservableList<Account> list;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        list = DBQueries.getAccounts(Main.getUserID());

        websiteColumn.setCellValueFactory(new PropertyValueFactory<Account, String>("website"));
        usernameColumn.setCellValueFactory(new PropertyValueFactory<Account, String>("username"));
        passColumn.setCellValueFactory(new PropertyValueFactory<Account, String>("pass"));
        // creates a new label inside the table cell to hide password
        passColumn.setCellFactory(x -> new PasswordLabelCell());
        urlColumn.setCellValueFactory(new PropertyValueFactory<Account, String>("url"));
        urlColumn.setCellFactory(column -> new ButtonCell("link-button") {
            @Override
            public void onClick() {
                int index = getTableRow().getIndex();
                Account a = accountTable.getItems().get(index);
                try {
                    Desktop.getDesktop().browse(new URL(a.getUrl()).toURI());
                    String pass = a.getPass();
                    copyPass(pass);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                } catch (URISyntaxException e) {
                    System.out.println(e.getMessage());
                }
            }
        });
        deleteColumn.setCellFactory(column -> new ButtonCell("delete-button") {
            @Override
            public void onClick() {
                int index = getTableRow().getIndex();
                Account a = accountTable.getItems().remove(index);
                DBQueries.deleteAccount(a);
                accountTable.refresh();
            }
        });
        accountTable.setItems(list);
        accountTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {
                    Account account = accountTable.getSelectionModel().getSelectedItem();
                    websiteTF.setText(account.getWebsite());
                    usernameTF.setText(account.getUsername());
                    passTF.setText(account.getPass());
                    urlTF.setText(account.getUrl());

                    btnSave.setText(account.getAccountID() + "");
                }
            }
        });
        btnSave.setTextFill(Color.TRANSPARENT);
    }

    private void copyPass(String pass) {
        StringSelection stringSelection = new StringSelection(pass);
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
    }

    @FXML
    public void save() {
        String website = websiteTF.getText();
        String username = usernameTF.getText();
        String pass = passTF.getText();

        int accountID = Integer.parseInt(btnSave.getText());
        if(accountID != 0) {
            Account account = getAccount(accountID);
            if(isValid()) {
                account.setUrl(urlTF.getText());
                account.setWebsite(website);
                account.setUsername(username);
                account.setPass(pass);

                DBQueries.editAccount(account);
            }
            else {
                showAlert(Alert.AlertType.ERROR, "Fields can not be empty!");
            }
        }
        else {
            if(isValid()) {
                Account a = new Account(Main.getUserID(), urlTF.getText(), website, username, pass);
                list.add(a);
                DBQueries.createAccount(a);
            } else {
                showAlert(Alert.AlertType.ERROR, "Fields can not be empty!");
            }
        }
        clearFields();
        accountTable.refresh();
    }

    private Account getAccount(int accountID) {
        for(Account account : list) {
            if(account.getAccountID() == accountID)
                return account;
        }
        return null;
    }

    private void clearFields() {
        urlTF.setText("");
        websiteTF.setText("");
        usernameTF.setText("");
        passTF.setText("");
        btnSave.setText(0 + "");
    }

    private boolean isValid() {
        if(urlTF.getText().isEmpty() || urlTF.getText().equals("http://")) return false;
        if (!urlTF.getText().startsWith("http://")) {
            urlTF.setText("http://" + urlTF.getText());
        }
        if(websiteTF.getText().isEmpty())   return false;
        if(usernameTF.getText().isEmpty())  return false;
        if(passTF.getText().isEmpty())      return false;
        return true;
    }

    private void showAlert(Alert.AlertType type, String text) {
        Alert a = new Alert(type, text);
        a.initModality(Modality.APPLICATION_MODAL); //This is so that you can't do anything in the app until you close the popup
        a.setHeaderText("Alert alert alert");
        a.showAndWait();
    }

    @FXML
    public void help() {
    }

    @FXML
    public void clearAll() {
        clearFields();
    }

}
