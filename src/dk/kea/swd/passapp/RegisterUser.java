package dk.kea.swd.passapp;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by Nikolai on 21.09.2015.
 */
public class RegisterUser{

    public RegisterUser(){

        Stage primaryStage = new Stage();

        primaryStage.setTitle("Create new account");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(2);
        grid.setVgap(2);
        grid.setPadding(new Insets(2, 2, 2, 2));


        Label userName    = new Label("Username: ");
        Label userPass    = new Label("Password: ");
        Label confirmPass = new Label("Confirm password: ");
        Label email       = new Label("Email: ");

        TextField userTextField        = new TextField();
        PasswordField passwordFieldOne = new PasswordField();
        PasswordField passwordFieldTwo = new PasswordField();
        TextField emailTextField       = new TextField();

        final Text actiontarget = new Text();

        Button registerButton = new Button("Register");

        registerButton.setOnAction(event -> {

            String user   = userTextField.getText();
            String pass1  = passwordFieldOne.getText();
            String pass2  = passwordFieldTwo.getText();
            String emailX = emailTextField.getText();

            int userID = DBQueries.checkUserExistance(user);

            if(userID == 0){

                if(pass1.equals(pass2))
                {
                    if(InputChecker.emailChecker(emailX))
                    {
                        DBQueries.createNewUser(user,pass1,emailX);
                        primaryStage.close();
                    }
                    else {
                        actiontarget.setFill(Color.FIREBRICK);
                        actiontarget.setText("Wrong email format");
                        emailTextField.requestFocus();
                    }

                }
                else {
                    actiontarget.setFill(Color.FIREBRICK);
                    actiontarget.setText("Passwords does not match");
                    passwordFieldOne.clear();
                    passwordFieldTwo.clear();
                    passwordFieldOne.requestFocus();
                }

            }
            else {

                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("Username " +user + " already exist! \n" +
                        "Please enter new username");
                userTextField.clear();
                userTextField.requestFocus();

            }


        });

        grid.add(userName, 0, 0);
        grid.add(userTextField, 1, 0);

        grid.add(userPass,0,1);
        grid.add(passwordFieldOne,1,1);

        grid.add(confirmPass,0,2);
        grid.add(passwordFieldTwo,1,2);

        grid.add(email,0,3);
        grid.add(emailTextField,1,3);

        grid.add(registerButton,1,5);

        grid.add(actiontarget,0,6);

        Scene scene = new Scene(grid,300,200);

        primaryStage.setScene(scene);

        primaryStage.show();

    }


}
