package dk.kea.swd.passapp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nikolai on 21.09.2015.
 */
public class InputChecker {

    public static boolean emailChecker(String email)
    {
        boolean emailChecker = false;

        Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher mat = pattern.matcher(email);

        return mat.matches();

    }
}
