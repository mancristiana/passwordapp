package dk.kea.swd.passapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    private static Stage primaryStage;
    private static int userID;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        loadLoginPage();
        //loadAccountsPage();
    }

    public static void loadLoginPage() {
        Login login = new Login(primaryStage);
    }

    public static void loadAccountsPage(){
        Parent root = null;
        try {
            root = FXMLLoader.load(Main.class.getResource("AccountsPage.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        primaryStage.setTitle("Accounts");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void setUserID(int userID) {
        Main.userID = userID;
    }

    public static int getUserID() {
        return userID;
    }
}