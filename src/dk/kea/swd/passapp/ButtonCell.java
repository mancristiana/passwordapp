package dk.kea.swd.passapp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;

/**
 * Created by Laura Gadola on 24-Sep-15.
 */
public abstract class ButtonCell extends TableCell<Account, String> {
    final Button cellButton = new Button();

    public ButtonCell(String id){
        cellButton.setId(id);
//        cellButton.setText(id);
        cellButton.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent t) {
                onClick();
            }
        });
    }

    //Display button if the row is not empty
    @Override
    protected void updateItem(String t, boolean empty) {
        //setTooltip(new Tooltip(t));
        setText(t);
        super.updateItem(t, empty);
        if(!empty){
            setGraphic(cellButton);
        }
    }

    public abstract void onClick();

}
