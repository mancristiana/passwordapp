package dk.kea.swd.passapp;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by Nikolai on 21.09.2015.
 */
public class Login {

    public static int user_id = 0;

    public Login(Stage primaryStage){

        primaryStage.setTitle("Login window");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);

        Text scenetitle = new Text("Welcome to PassApp");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userNameLabel = new Label("User Name:");
        grid.add(userNameLabel, 0, 1);

        TextField userName = new TextField();
        grid.add(userName, 1, 1);

        Label pwLabel = new Label("Password:");
        grid.add(pwLabel, 0, 2);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        Button loginButton   = new Button("Sign in");
        Button registerButton = new Button("Register");
        registerButton.setVisible(false);
        VBox vertical = new VBox(10);
        vertical.setAlignment(Pos.BOTTOM_RIGHT);
        vertical.getChildren().add(loginButton);
        vertical.getChildren().add(registerButton);
        grid.add(vertical, 1, 4);

        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);


        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                String user = userName.getText();
                String pw = pwBox.getText();
                int userID = DBQueries.checkUserExistance(user);
                if (userID > 0) { //User exists
                    Boolean rightLogin = DBQueries.checkPassWord(user, pw);
                    if (rightLogin) {
                        Main.setUserID(userID);
                        Main.loadAccountsPage();

                    } else {
                        actiontarget.setFill(Color.FIREBRICK);
                        actiontarget.setText("Wrong password.\nReenter your password");
                        pwBox.setText("");
                        pwBox.requestFocus();
                    }
                } else {
                    actiontarget.setFill(Color.FIREBRICK);
                    actiontarget.setText("User does not exist.\nPush 'Register to create account.");
                    registerButton.setVisible(true);
                }

            }
        });

        registerButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {

                RegisterUser registration = new RegisterUser();
            }
        });

        primaryStage.show();

    }
}
