package dk.kea.swd.passapp;

/**
 * Created by Cris on 21-Sep-15.
 */
public class Account {
    private int userID;
    private int accountID;
    private String url;
    private String website;
    private String username;
    private String pass;

    public Account(int userID, String url, String website, String username, String pass) {
        this.userID = userID;
        this.accountID = 0;
        this.url = url;
        this.website = website;
        this.username = username;
        this.pass = pass;
    }

    public Account(int accountID, int userID, String url, String website, String username, String pass) {
        this.userID = userID;
        this.accountID = accountID;
        this.url = url;
        this.website = website;
        this.username = username;
        this.pass = pass;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    @Override
    public String toString() {
        return "Account{" +
                "userID=" + userID +
                ", url='" + url + '\'' +
                ", website='" + website + '\'' +
                ", username='" + username + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }
}
