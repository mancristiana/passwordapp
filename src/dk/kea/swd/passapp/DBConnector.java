package dk.kea.swd.passapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {

//    private static final String USER = "root";
//    private static final String PASS = "";
//    private static final String DB = "passapp";
//    private static final String URL = "jdbc:mysql://localhost";
//    private static final String PORT = "3306";

    private static final String USER = "datgroup1";
    private static final String PASS = "passapp";
    private static final String DB = "passwordapp";
    private static final String URL = "jdbc:mysql://martynasjankauskasdb.cpaqukuiezel.us-west-2.rds.amazonaws.com";
    private static final String PORT = "3306";

    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String urlForConn = URL + ":" + PORT + "/" + DB;
            con = DriverManager.getConnection(urlForConn, USER, PASS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }
}